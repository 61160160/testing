const assert = require('assert')
const { checkEnableTime } = require('../JobPosting')
describe('JobPosting', function () {
  describe('checkEnableTime', function(){
    it('shoud return true when เวลาสมัครอยู่ในช่วงที่รับการสมัคร', function () {
      const startTime = new Date(2021, 2, 10)
      const endTime = new Date(2021, 2, 14)
      const today = new Date(2021, 2, 11)
      const expectedResult = true;
      const actualResult = checkEnableTime(startTime, endTime, today)
      assert.strictEqual(actualResult, expectedResult)
    })
    it('shoud return true when เวลาสมัครตรงกับวันที่เปิดสมัคร', function () {
      const startTime = new Date(2021, 2, 10)
      const endTime = new Date(2021, 2, 14)
      const today = new Date(2021, 2, 10)
      const expectedResult = true;
  
      const actualResult = checkEnableTime(startTime, endTime, today)
  
      assert.strictEqual(actualResult, expectedResult)
    })
    it('shoud return true when เวลาสมัครตรงกับเวลาวันที่ปิดสมัคร', function () {
      const startTime = new Date(2021, 2, 10)
      const endTime = new Date(2021, 2, 14)
      const today = new Date(2021, 2, 14)
      const expectedResult = true;
  
      const actualResult = checkEnableTime(startTime, endTime, today)
  
      assert.strictEqual(actualResult, expectedResult)
    })
    it('shoud return false when เวลาสมัครอยู่ก่อนเวลาเริ่มต้น', function () {
      const startTime = new Date(2021, 2, 10)
      const endTime = new Date(2021, 2, 14)
      const today = new Date(2021, 2, 1)
      const expectedResult = false;
  
      const actualResult = checkEnableTime(startTime, endTime, today)
  
      assert.strictEqual(actualResult, expectedResult)
    })
    it('shoud return false when เวลาสมัครเลยเวลารับสมัคร', function () {
      const startTime = new Date(2021, 2, 10)
      const endTime = new Date(2021, 2, 14)
      const today = new Date(2021, 2, 15)
      const expectedResult = false;
  
      const actualResult = checkEnableTime(startTime, endTime, today)
  
      assert.strictEqual(actualResult, expectedResult)
    })
  })
})









