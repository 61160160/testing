exports.checkEnableTime = function (startTime, endtime, today) {
  if(today < startTime) {
    return false
  }
  if(today > endtime) {
    return false
  }
  return true
}